namespace Scheduler
{
    partial class ScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SubmitButton = new System.Windows.Forms.Button();
            this.IDLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.EventLabel = new System.Windows.Forms.Label();
            this.IDTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.EventComboBox = new System.Windows.Forms.ComboBox();
            this.StudentGroupBox = new System.Windows.Forms.GroupBox();
            this.ClearButton = new System.Windows.Forms.Button();
            this.MeetingGroupBox = new System.Windows.Forms.GroupBox();
            this.AdvisorComboBox = new System.Windows.Forms.ComboBox();
            this.AdvisorCheckBox = new System.Windows.Forms.CheckBox();
            this.TimeGroupBox = new System.Windows.Forms.GroupBox();
            this.TimeComboBox = new System.Windows.Forms.ComboBox();
            this.ScheduleCalendar = new System.Windows.Forms.MonthCalendar();
            this.RefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.PhoneNumTextBox = new System.Windows.Forms.TextBox();
            this.PhoneNumLabel = new System.Windows.Forms.Label();
            this.ContactInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.EmailAddressLabel = new System.Windows.Forms.Label();
            this.EmailAddressTextBox = new System.Windows.Forms.TextBox();
            this.StudentGroupBox.SuspendLayout();
            this.MeetingGroupBox.SuspendLayout();
            this.TimeGroupBox.SuspendLayout();
            this.ContactInfoGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // SubmitButton
            // 
            this.SubmitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SubmitButton.Enabled = false;
            this.SubmitButton.Location = new System.Drawing.Point(400, 331);
            this.SubmitButton.Name = "SubmitButton";
            this.SubmitButton.Size = new System.Drawing.Size(75, 23);
            this.SubmitButton.TabIndex = 0;
            this.SubmitButton.Text = "Submit";
            this.SubmitButton.UseVisualStyleBackColor = true;
            this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
            // 
            // IDLabel
            // 
            this.IDLabel.AutoSize = true;
            this.IDLabel.Location = new System.Drawing.Point(6, 16);
            this.IDLabel.Name = "IDLabel";
            this.IDLabel.Size = new System.Drawing.Size(58, 13);
            this.IDLabel.TabIndex = 1;
            this.IDLabel.Text = "Student ID";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(6, 59);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(35, 13);
            this.NameLabel.TabIndex = 2;
            this.NameLabel.Text = "Name";
            // 
            // EventLabel
            // 
            this.EventLabel.AutoSize = true;
            this.EventLabel.Location = new System.Drawing.Point(6, 70);
            this.EventLabel.Name = "EventLabel";
            this.EventLabel.Size = new System.Drawing.Size(46, 13);
            this.EventLabel.TabIndex = 3;
            this.EventLabel.Text = "Purpose";
            // 
            // IDTextBox
            // 
            this.IDTextBox.Location = new System.Drawing.Point(6, 32);
            this.IDTextBox.Name = "IDTextBox";
            this.IDTextBox.Size = new System.Drawing.Size(200, 20);
            this.IDTextBox.TabIndex = 5;
            this.IDTextBox.TextChanged += new System.EventHandler(this.IDTextBox_TextChanged);
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(6, 75);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(200, 20);
            this.NameTextBox.TabIndex = 6;
            this.NameTextBox.TextChanged += new System.EventHandler(this.NameTextBox_TextChanged);
            this.NameTextBox.Enter += new System.EventHandler(this.NameTextBox_Enter);
            // 
            // EventComboBox
            // 
            this.EventComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EventComboBox.FormattingEnabled = true;
            this.EventComboBox.Location = new System.Drawing.Point(6, 86);
            this.EventComboBox.Name = "EventComboBox";
            this.EventComboBox.Size = new System.Drawing.Size(200, 21);
            this.EventComboBox.TabIndex = 7;
            this.EventComboBox.SelectedIndexChanged += new System.EventHandler(this.EventComboBox_SelectedIndexChanged);
            this.EventComboBox.EnabledChanged += new System.EventHandler(this.EventComboBox_EnabledChanged);
            // 
            // StudentGroupBox
            // 
            this.StudentGroupBox.Controls.Add(this.IDLabel);
            this.StudentGroupBox.Controls.Add(this.IDTextBox);
            this.StudentGroupBox.Controls.Add(this.NameTextBox);
            this.StudentGroupBox.Controls.Add(this.NameLabel);
            this.StudentGroupBox.Location = new System.Drawing.Point(18, 133);
            this.StudentGroupBox.Name = "StudentGroupBox";
            this.StudentGroupBox.Size = new System.Drawing.Size(212, 102);
            this.StudentGroupBox.TabIndex = 8;
            this.StudentGroupBox.TabStop = false;
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(20, 331);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 7;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // MeetingGroupBox
            // 
            this.MeetingGroupBox.Controls.Add(this.AdvisorComboBox);
            this.MeetingGroupBox.Controls.Add(this.AdvisorCheckBox);
            this.MeetingGroupBox.Controls.Add(this.EventLabel);
            this.MeetingGroupBox.Controls.Add(this.EventComboBox);
            this.MeetingGroupBox.Enabled = false;
            this.MeetingGroupBox.Location = new System.Drawing.Point(18, 12);
            this.MeetingGroupBox.Name = "MeetingGroupBox";
            this.MeetingGroupBox.Size = new System.Drawing.Size(212, 115);
            this.MeetingGroupBox.TabIndex = 9;
            this.MeetingGroupBox.TabStop = false;
            // 
            // AdvisorComboBox
            // 
            this.AdvisorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AdvisorComboBox.Enabled = false;
            this.AdvisorComboBox.FormattingEnabled = true;
            this.AdvisorComboBox.Location = new System.Drawing.Point(6, 42);
            this.AdvisorComboBox.Name = "AdvisorComboBox";
            this.AdvisorComboBox.Size = new System.Drawing.Size(200, 21);
            this.AdvisorComboBox.TabIndex = 9;
            this.AdvisorComboBox.SelectedIndexChanged += new System.EventHandler(this.AdvisorComboBox_SelectedIndexChanged);
            this.AdvisorComboBox.EnabledChanged += new System.EventHandler(this.AdvisorComboBox_EnabledChanged);
            // 
            // AdvisorCheckBox
            // 
            this.AdvisorCheckBox.AutoSize = true;
            this.AdvisorCheckBox.Location = new System.Drawing.Point(9, 19);
            this.AdvisorCheckBox.Name = "AdvisorCheckBox";
            this.AdvisorCheckBox.Size = new System.Drawing.Size(61, 17);
            this.AdvisorCheckBox.TabIndex = 8;
            this.AdvisorCheckBox.Text = "Advisor";
            this.AdvisorCheckBox.UseVisualStyleBackColor = true;
            this.AdvisorCheckBox.CheckedChanged += new System.EventHandler(this.AdvisorCheckBox_CheckedChanged);
            this.AdvisorCheckBox.EnabledChanged += new System.EventHandler(this.AdvisorCheckBox_EnabledChanged);
            // 
            // TimeGroupBox
            // 
            this.TimeGroupBox.Controls.Add(this.TimeComboBox);
            this.TimeGroupBox.Controls.Add(this.ScheduleCalendar);
            this.TimeGroupBox.Enabled = false;
            this.TimeGroupBox.Location = new System.Drawing.Point(230, 12);
            this.TimeGroupBox.Name = "TimeGroupBox";
            this.TimeGroupBox.Size = new System.Drawing.Size(249, 223);
            this.TimeGroupBox.TabIndex = 10;
            this.TimeGroupBox.TabStop = false;
            // 
            // TimeComboBox
            // 
            this.TimeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TimeComboBox.FormattingEnabled = true;
            this.TimeComboBox.Location = new System.Drawing.Point(10, 194);
            this.TimeComboBox.Name = "TimeComboBox";
            this.TimeComboBox.Size = new System.Drawing.Size(227, 21);
            this.TimeComboBox.TabIndex = 1;
            this.TimeComboBox.SelectedIndexChanged += new System.EventHandler(this.TimeComboBox_SelectedIndexChanged);
            this.TimeComboBox.EnabledChanged += new System.EventHandler(this.TimeComboBox_EnabledChanged);
            // 
            // ScheduleCalendar
            // 
            this.ScheduleCalendar.BackColor = System.Drawing.SystemColors.Window;
            this.ScheduleCalendar.Location = new System.Drawing.Point(10, 19);
            this.ScheduleCalendar.MaxSelectionCount = 1;
            this.ScheduleCalendar.Name = "ScheduleCalendar";
            this.ScheduleCalendar.TabIndex = 0;
            this.ScheduleCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.ScheduleCalendar_DateChanged);
            this.ScheduleCalendar.EnabledChanged += new System.EventHandler(this.ScheduleCalendar_EnabledChanged);
            // 
            // RefreshTimer
            // 
            this.RefreshTimer.Interval = 1000;
            this.RefreshTimer.Tick += new System.EventHandler(this.RefreshTimer_Tick);
            // 
            // PhoneNumTextBox
            // 
            this.PhoneNumTextBox.Location = new System.Drawing.Point(4, 40);
            this.PhoneNumTextBox.Name = "PhoneNumTextBox";
            this.PhoneNumTextBox.Size = new System.Drawing.Size(200, 20);
            this.PhoneNumTextBox.TabIndex = 11;
            // 
            // PhoneNumLabel
            // 
            this.PhoneNumLabel.AutoSize = true;
            this.PhoneNumLabel.Location = new System.Drawing.Point(6, 22);
            this.PhoneNumLabel.Name = "PhoneNumLabel";
            this.PhoneNumLabel.Size = new System.Drawing.Size(78, 13);
            this.PhoneNumLabel.TabIndex = 12;
            this.PhoneNumLabel.Text = "Phone Number";
            // 
            // ContactInfoGroupBox
            // 
            this.ContactInfoGroupBox.Controls.Add(this.EmailAddressLabel);
            this.ContactInfoGroupBox.Controls.Add(this.EmailAddressTextBox);
            this.ContactInfoGroupBox.Controls.Add(this.PhoneNumLabel);
            this.ContactInfoGroupBox.Controls.Add(this.PhoneNumTextBox);
            this.ContactInfoGroupBox.Location = new System.Drawing.Point(20, 250);
            this.ContactInfoGroupBox.Name = "ContactInfoGroupBox";
            this.ContactInfoGroupBox.Size = new System.Drawing.Size(458, 75);
            this.ContactInfoGroupBox.TabIndex = 13;
            this.ContactInfoGroupBox.TabStop = false;
            this.ContactInfoGroupBox.Text = "Contact Information (Optional)";
            // 
            // EmailAddressLabel
            // 
            this.EmailAddressLabel.AutoSize = true;
            this.EmailAddressLabel.Location = new System.Drawing.Point(217, 16);
            this.EmailAddressLabel.Name = "EmailAddressLabel";
            this.EmailAddressLabel.Size = new System.Drawing.Size(73, 13);
            this.EmailAddressLabel.TabIndex = 14;
            this.EmailAddressLabel.Text = "Email Address";
            // 
            // EmailAddressTextBox
            // 
            this.EmailAddressTextBox.Location = new System.Drawing.Point(220, 40);
            this.EmailAddressTextBox.Name = "EmailAddressTextBox";
            this.EmailAddressTextBox.Size = new System.Drawing.Size(221, 20);
            this.EmailAddressTextBox.TabIndex = 13;
            // 
            // ScheduleForm
            // 
            this.AcceptButton = this.SubmitButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 366);
            this.Controls.Add(this.ContactInfoGroupBox);
            this.Controls.Add(this.TimeGroupBox);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.MeetingGroupBox);
            this.Controls.Add(this.StudentGroupBox);
            this.Controls.Add(this.SubmitButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ScheduleForm";
            this.Text = "Scheduler";
            this.Deactivate += new System.EventHandler(this.ScheduleForm_Deactivate);
            this.Load += new System.EventHandler(this.ScheduleForm_Load);
            this.StudentGroupBox.ResumeLayout(false);
            this.StudentGroupBox.PerformLayout();
            this.MeetingGroupBox.ResumeLayout(false);
            this.MeetingGroupBox.PerformLayout();
            this.TimeGroupBox.ResumeLayout(false);
            this.ContactInfoGroupBox.ResumeLayout(false);
            this.ContactInfoGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SubmitButton;
        private System.Windows.Forms.Label IDLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label EventLabel;
        private System.Windows.Forms.TextBox IDTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.ComboBox EventComboBox;
        private System.Windows.Forms.GroupBox StudentGroupBox;
        private System.Windows.Forms.GroupBox MeetingGroupBox;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.ComboBox AdvisorComboBox;
        private System.Windows.Forms.CheckBox AdvisorCheckBox;
        private System.Windows.Forms.GroupBox TimeGroupBox;
        private System.Windows.Forms.ComboBox TimeComboBox;
        private System.Windows.Forms.MonthCalendar ScheduleCalendar;
        private System.Windows.Forms.Timer RefreshTimer;
        private System.Windows.Forms.Label PhoneNumLabel;
        private System.Windows.Forms.TextBox PhoneNumTextBox;
        private System.Windows.Forms.GroupBox ContactInfoGroupBox;
        private System.Windows.Forms.Label EmailAddressLabel;
        private System.Windows.Forms.TextBox EmailAddressTextBox;
    }
}

