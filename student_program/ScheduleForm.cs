using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Scheduler
{

    public partial class ScheduleForm : Form
    {

        public ScheduleForm()
        {
            InitializeComponent();
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                cxn.Open();
                Global.oAdvisorList = new List<Advisor>();
                using (var cmd = new System.Data.SqlClient.SqlCommand("Queries have been ommitted for security purposes ", cxn))
                {
                    var rs = cmd.ExecuteReader();
                    while (rs.Read())
                    {
                        Global.oAdvisorList.Add(new Advisor(Convert.ToInt32(rs[0]), Convert.ToString(rs[1]), Convert.ToBoolean(rs[2])));
                    }
                    rs.Close();
                }
                Global.oEventList = new List<Event>();
                using (var cmd = new System.Data.SqlClient.SqlCommand("Queries have been ommitted for security purposes ", cxn))
                {
                    var rs = cmd.ExecuteReader();
                    while (rs.Read())
                    {
                        Global.oEventList.Add(new Event(Convert.ToInt32(rs[0]), Convert.ToInt32(rs[1]), Convert.ToString(rs[2])));
                    }
                    rs.Close();
                }
                HighestMeetingID_Update(cxn);
                using (var cmd = new SqlCommand("Queries have been ommitted for security purposes ", cxn))
                {
                    var rs = cmd.ExecuteReader();
                    while (rs.Read())
                    {
                        switch (Convert.ToString(rs[0]))
                        {
                            case "Schedule Interval": Global.iScheduleInterval = Convert.ToInt32(rs[1]); break;
                            case "Day Start Time": Global.iDayStartTime = Convert.ToInt32(rs[1]); break;
                            case "Day End Time": Global.iDayEndTime = Convert.ToInt32(rs[1]); break;
                            case "Free Meeting": Global.iFreeMeeting = Convert.ToInt32(rs[1]); break;
                        }
                    }
                    rs.Close();
                }
                cxn.Close();
            }
        }

        private void ScheduleForm_Load(object sender, EventArgs e)
        {
            foreach (var o in Global.oAdvisorList) AdvisorComboBox.Items.Add(o.DisplayText);
            foreach (var o in Global.oEventList) EventComboBox.Items.Add(o.DisplayText);
            ScheduleCalendar.MinDate = DateTime.Today;
            Global.StartListener();
            RefreshTimer.Enabled = true;
        }

        private void ScheduleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Global.StopListener();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            IDTextBox.Text = NameTextBox.Text = PhoneNumTextBox.Text =EmailAddressTextBox.Text = String.Empty;
        }

        private void IDTextBox_TextChanged(object sender, EventArgs e)
        {
            int c = IDTextBox.SelectionStart;
            for (int i = IDTextBox.Text.Length - 1; i >= 0; i--)
            {
                if (IDTextBox.Text[i] < '0' || IDTextBox.Text[i] > '9')
                {
                    IDTextBox.Text = IDTextBox.Text.Remove(i, 1);
                    if (c > 0) c--;
                }
            }
            IDTextBox.SelectionStart = c;
        }

        private void NameTextBox_Enter(object sender, EventArgs e)
        {
            if (IDTextBox.Text.Length > 0)
            {
                using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                {
                    cxn.Open();
                    using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + IDTextBox.Text + ";", cxn))
                    {
                        NameTextBox.Text = Convert.ToString(cmd.ExecuteScalar());
                    }
                    cxn.Close();
                }
            }
        }

        private void EmailAddressTextBox_Enter(object sender, EventArgs e)
        {
            if (IDTextBox.Text.Length > 0)
            {
                using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                {
                    cxn.Open();
                    using ( var cmd = new SqlCommand("Queries have been ommitted for security purposes  " + IDTextBox.Text +";", cxn))
                    {   
                        EmailAddressTextBox.Text = Convert.ToString(cmd.ExecuteScalar());
                    }
                    cxn.Close();
                }
            }
        }
        
        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {
            MeetingGroupBox.Enabled = NameTextBox.Text.Length > 0;
        }

        private void AdvisorCheckBox_EnabledChanged(object sender, EventArgs e)
        {
            if (AdvisorCheckBox.Enabled == false) AdvisorCheckBox.Checked = false;
        }

        private void AdvisorCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            AdvisorComboBox.Enabled = AdvisorCheckBox.Checked;
        }

        private void AdvisorComboBox_EnabledChanged(object sender, EventArgs e)
        {
            if (AdvisorComboBox.Enabled == false)
            {
                AdvisorComboBox.SelectedIndex = -1;
                if (EventComboBox.SelectedIndex >= 0)
                {
                    using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                    {
                        cxn.Open();
                        TimeComboBox_Update(cxn);
                        cxn.Close();
                    }
                }
            }
        }

        private void AdvisorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AdvisorComboBox.SelectedIndex >= 0)
            {
                using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                {
                    cxn.Open();
                    AvailableSlots_Update(cxn);
                    if (EventComboBox.SelectedIndex >= 0) TimeComboBox_Update(cxn);
                    cxn.Close();
                }
            }
        }

        private void EventComboBox_EnabledChanged(object sender, EventArgs e)
        {
            if (EventComboBox.Enabled == false) EventComboBox.SelectedIndex = -1;
        }

        private void EventComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (EventComboBox.SelectedIndex >= 0)
            {
                TimeGroupBox.Enabled = true;
                using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                {
                    cxn.Open();
                    TimeComboBox_Update(cxn);
                    cxn.Close();
                }
            }
            else
            {
                TimeGroupBox.Enabled = false;
            }
        }

        private void ScheduleCalendar_EnabledChanged(object sender, EventArgs e)
        {
            if (ScheduleCalendar.Enabled == false) 
                ScheduleCalendar.SelectionStart = DateTime.Today;
        }

        private void ScheduleCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            if (ScheduleCalendar.Enabled)
            {
                using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                {
                    cxn.Open();
                    TimeComboBox_Update(cxn);
                    cxn.Close();
                }
            }
        }

        private void TimeComboBox_EnabledChanged(object sender, EventArgs e)
        {
            if (TimeComboBox.Enabled == false) TimeComboBox.SelectedIndex = -1;
        }

        private void TimeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SubmitButton.Enabled = TimeComboBox.SelectedIndex >= 0;
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                if (ValidEmailForm())
                {
                    cxn.Open();
                    using (var cmd = new SqlCommand("Queries have been ommitted for security purposes  " + Convert.ToString(Global.oEventList[EventComboBox.SelectedIndex].ID) + ", " + (IDTextBox.Text.Length == 0 ? "0" : IDTextBox.Text) + ", '" + NameTextBox.Text.Replace("'", "''") + "', " + ((Slot)TimeComboBox.Items[TimeComboBox.SelectedIndex]).Advisors[0] + ", '" + ((Slot)TimeComboBox.Items[TimeComboBox.SelectedIndex]).StartTime + PhoneNumTextBox.Text + EmailAddressTextBox.Text + "';", cxn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    cxn.Open();
                    using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + Convert.ToString(Global.oEventList[EventComboBox.SelectedIndex].ID) + ", " + (IDTextBox.Text.Length == 0 ? "0" : IDTextBox.Text) + ", '" + NameTextBox.Text.Replace("'", "''") + "', " + ((Slot)TimeComboBox.Items[TimeComboBox.SelectedIndex]).Advisors[0] + ", '" + ((Slot)TimeComboBox.Items[TimeComboBox.SelectedIndex]).StartTime + PhoneNumTextBox.Text + "" + "';", cxn))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
                
                   
                int i = 0;
                while (Global.oAdvisorList[i].ID != ((Slot)TimeComboBox.Items[TimeComboBox.SelectedIndex]).Advisors[0]) i++;
                DateTime d = ((Slot)TimeComboBox.Items[TimeComboBox.SelectedIndex]).StartTime;
                using (var n = new UdpClient())
                {
                    byte[] bytes = Encoding.ASCII.GetBytes("Schedule");
                    n.Send(bytes, bytes.Length, new IPEndPoint(IPAddress.Broadcast, Global.iSendPort));
                    n.Close();
                }
                MessageBox.Show("Meeting scheduled with " + Global.oAdvisorList[i].DisplayText + " on " + Convert.ToString(d.Month) + "/" + Convert.ToString(d.Day) + " at " + Convert.ToString(d.Hour > 12 ? d.Hour - 12 : d.Hour) + ":" + (d.Minute < 10 ? "0" : "") + Convert.ToString(d.Minute) + ".");


                if (EmailAddressTextBox.Text != null)
                {
                    if (ValidEmailForm())
                    {
                        MailConfirmation();
                    }

                }
                IDTextBox.Text = NameTextBox.Text = PhoneNumTextBox.Text = EmailAddressTextBox.Text = String.Empty;
            }
        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            Global.iTickCount++;
            if (TimeComboBox.Enabled)
            {
                if (Global.fUpdate || Global.iTickCount == 60)
                {
                    using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                    {
                        cxn.Open();
                        if (HighestMeetingID_Update(cxn) || AvailableSlots_Update(cxn)) TimeComboBox_Update(cxn);
                        cxn.Close();
                    }
                    if (ScheduleCalendar.SelectionStart < DateTime.Today) ScheduleCalendar.SelectionStart = DateTime.Today;
                    if (ScheduleCalendar.MinDate < DateTime.Today) ScheduleCalendar.MinDate = DateTime.Today;
                }
            }
            if (Global.iTickCount == 60) Global.iTickCount = 0;
        }

        private bool AvailableSlots_Update(SqlConnection cxn)
        {
            int i = Global.iAvailableSlots;
            using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + (AdvisorComboBox.SelectedIndex >= 0 ? " [AdvisorID] = " + Convert.ToString(Global.oAdvisorList[AdvisorComboBox.SelectedIndex].ID) + " AND" : "") + " [MeetingID] = " + Convert.ToString(Global.iFreeMeeting) + ";", cxn))
            {
                Global.iAvailableSlots = Convert.ToInt32(cmd.ExecuteScalar());
            }
            return i != Global.iAvailableSlots;
        }

        private bool HighestMeetingID_Update(SqlConnection cxn)
        {
            int i = Global.iHighestMeetingID;
            using (var cmd = new SqlCommand("Queries have been ommitted for security purposes ", cxn))
            {
                Global.iHighestMeetingID = Convert.ToInt32(cmd.ExecuteScalar());
            }
            return i != Global.iHighestMeetingID;
        }

        private void TimeComboBox_Update(SqlConnection cxn)
        {
            TimeComboBox.Items.Clear();
            SubmitButton.Enabled = false;
            var l = Global.oEventList[EventComboBox.SelectedIndex].RequiredTime / Global.iScheduleInterval;
            var a = new List<int>();
            if (AdvisorComboBox.SelectedIndex >= 0)
            {
                a.Add(Global.oAdvisorList[AdvisorComboBox.SelectedIndex].ID);
            }
            else 
                foreach (var o in Global.oAdvisorList)
                    if(o.GeneralAdvising)
                        a.Add(o.ID);
            foreach (var id in a)
            {
                var t = new List<DateTime>();
                using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + Convert.ToString(Global.iFreeMeeting) + " AND [TimeSlot] > '" + Convert.ToString(ScheduleCalendar.SelectionStart) + "' AND [TimeSlot] < '" + Convert.ToString(ScheduleCalendar.SelectionStart.AddDays(1.0)) + "' AND [AdvisorID] = " + id + ";", cxn))
                {
                    var rs = cmd.ExecuteReader();
                    while (rs.Read())
                    {
                        t.Add(Convert.ToDateTime(rs[0]));
                    }
                    rs.Close();
                }
                for (int i = 0; i + l - 1 < t.Count; i++)
                {
                    bool f = true;
                    for (int j = 1; j < l; j++)
                    {
                        if (t[i].AddMinutes(Global.iScheduleInterval * j) < t[i + j]) f = false;
                    }
                    if (f)
                    {
                        f = true;
                        for (int j = 0; j < TimeComboBox.Items.Count; j++)
                        {
                            Slot s = (Slot)TimeComboBox.Items[j];
                            if (s.StartTime == t[i])
                            {
                                s.Advisors.Add(id);
                                f = false;
                                break;
                            }
                            else if (s.StartTime > t[i])
                            {
                                TimeComboBox.Items.Insert(j, new Slot(t[i], id));
                                f = false;
                                break;
                            }
                        }
                        if (f)
                        {
                            TimeComboBox.Items.Add(new Slot(t[i], id));
                        }
                    }
                }
            }
        }

        private void ScheduleForm_Deactivate(object sender, EventArgs e)
        {
            Global.StopListener();
        }

        private void PhoneNumTextBox_TextChanged(object sender, EventArgs e)
        {
            int c = PhoneNumTextBox.SelectionStart;
            for (int i = PhoneNumTextBox.Text.Length - 1; i >= 0; i--)
            {
                if (PhoneNumTextBox.Text[i] < '0' || PhoneNumTextBox.Text[i] > '9' || PhoneNumTextBox.Text[i] == '-' || PhoneNumTextBox.Text[i] == '(' ||PhoneNumTextBox.Text[i] ==')' || PhoneNumTextBox.Text[i] == ' ' )
                {
                    PhoneNumTextBox.Text = PhoneNumTextBox.Text.Remove(i, 1);
                    if (c > 0) c--;
                }
            }
            PhoneNumTextBox.SelectionStart = c;
        }
        private bool ValidEmailForm()
        {
            return Regex.IsMatch(EmailAddressTextBox.Text, @"^\G(\w+'+\w+?@\w+?\x2E.+)$");
            
        }

        private void MailConfirmation()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("mail.nwic.edu");
                DateTime d = ((Slot)TimeComboBox.Items[TimeComboBox.SelectedIndex]).StartTime;
                mail.From = new MailAddress("noreply@nwic.edu");
                mail.To.Add(EmailAddressTextBox.Text);
                mail.Subject = "DO NOT REPLY: Confirmation of Appointment";
                mail.Body = ("This is a confirmation of your appointment with " + AdvisorComboBox.ToString() + ".\n" + "Appointment is scheduled for " + Convert.ToString(d) + "\n\n" + "This email is automatically generated, please do not reply to it.");
                SmtpServer.Send(mail);

                MessageBox.Show("A confirmation email has been sent to " + EmailAddressTextBox.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        

    }

    class Global
    {

        public const int iReceivePort = 121, iSendPort = 122;

        public const string strDatabaseConnectionString = "server= ommitted";

        public static Listener oListener;
        public static Thread oThread;

        public static bool fUpdate = false;
        public static int iTickCount = 0;

        public static int iScheduleInterval;
        public static int iDayStartTime, iDayEndTime;
        public static int iFreeMeeting;

        public static int iAvailableSlots;
        public static int iHighestMeetingID;

        public static List<Advisor> oAdvisorList;
        public static List<Event> oEventList;

        public static void StartListener()
        {
            oListener = new Listener();
            oThread = new Thread(new ThreadStart(oListener.Run));
            oThread.Start();
        }

        public static void StopListener()
        {
            oThread.Abort();
        }

        
    }

    class Advisor
    {
        public int ID; public string DisplayText;
        public bool GeneralAdvising;

        public Advisor(int ID, string DisplayText, bool GeneralAdvising) { this.ID = ID; this.DisplayText = DisplayText; this.GeneralAdvising = GeneralAdvising; }
    }

    class Event
    {
        public int ID; public int RequiredTime; public string DisplayText;
        public Event(int ID, int RequiredTime, string DisplayText) { this.ID = ID; this.RequiredTime = RequiredTime; this.DisplayText = DisplayText; }
    }

    class Slot
    {
        public DateTime StartTime;
        public List<int> Advisors;
        public Slot(DateTime StartTime, int Advisor) { this.StartTime = StartTime; Advisors = new List<int>(); Advisors.Add(Advisor); }
        public override string ToString()
        {
            return Convert.ToString(StartTime.Hour > 12 ? StartTime.Hour - 12 : StartTime.Hour) + ":" + (StartTime.Minute < 10 ? "0" : "") + Convert.ToString(StartTime.Minute) + (StartTime.Hour > 13 ? " PM" : " AM");
        }
    }

    public class Listener
    {
        public void Run()
        {
            try
            {
                using (var c = new UdpClient(Global.iReceivePort))
                {
                    for (;;)
                    {
                        if (c.Available > 0)
                        {
                            var a = new IPEndPoint(IPAddress.Any, Global.iReceivePort);
                            byte[] d = c.Receive(ref a);
                            string s = Encoding.ASCII.GetString(d);
                            switch (s)
                            {
                                case "Available":
                                case "Unavailable":
                                case "Cancel":
                                    Global.fUpdate = true;
                                    break;
                            }
                        }
                        else
                        {
                            Thread.Sleep(100);
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }

}
