namespace Meeting_Schedule
{
    partial class ScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ScheduleListView = new System.Windows.Forms.ListView();
            this.TimeColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IDColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.NameColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.EventColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ScheduleCalendar = new System.Windows.Forms.MonthCalendar();
            this.CancelCheckBox = new System.Windows.Forms.CheckBox();
            this.AvailableButton = new System.Windows.Forms.Button();
            this.UnavailableButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.RefreshTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ScheduleListView
            // 
            this.ScheduleListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ScheduleListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.TimeColumnHeader,
            this.IDColumnHeader,
            this.NameColumnHeader,
            this.EventColumnHeader});
            this.ScheduleListView.FullRowSelect = true;
            this.ScheduleListView.GridLines = true;
            this.ScheduleListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ScheduleListView.Location = new System.Drawing.Point(251, 12);
            this.ScheduleListView.Name = "ScheduleListView";
            this.ScheduleListView.Size = new System.Drawing.Size(453, 395);
            this.ScheduleListView.TabIndex = 0;
            this.ScheduleListView.UseCompatibleStateImageBehavior = false;
            this.ScheduleListView.View = System.Windows.Forms.View.Details;
            // 
            // TimeColumnHeader
            // 
            this.TimeColumnHeader.Text = "Time";
            // 
            // IDColumnHeader
            // 
            this.IDColumnHeader.Text = "ID";
            // 
            // NameColumnHeader
            // 
            this.NameColumnHeader.Text = "Name";
            this.NameColumnHeader.Width = 145;
            // 
            // EventColumnHeader
            // 
            this.EventColumnHeader.Text = "Purpose";
            this.EventColumnHeader.Width = 139;
            // 
            // ScheduleCalendar
            // 
            this.ScheduleCalendar.Location = new System.Drawing.Point(12, 12);
            this.ScheduleCalendar.Name = "ScheduleCalendar";
            this.ScheduleCalendar.TabIndex = 1;
            this.ScheduleCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.ScheduleCalendar_DateChanged);
            // 
            // CancelCheckBox
            // 
            this.CancelCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.CancelCheckBox.AutoSize = true;
            this.CancelCheckBox.Location = new System.Drawing.Point(18, 419);
            this.CancelCheckBox.Name = "CancelCheckBox";
            this.CancelCheckBox.Size = new System.Drawing.Size(172, 17);
            this.CancelCheckBox.TabIndex = 2;
            this.CancelCheckBox.Text = "Allow meetings to be cancelled";
            this.CancelCheckBox.UseVisualStyleBackColor = true;
            this.CancelCheckBox.CheckedChanged += new System.EventHandler(this.CancelCheckBox_CheckedChanged);
            // 
            // AvailableButton
            // 
            this.AvailableButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.AvailableButton.Location = new System.Drawing.Point(251, 413);
            this.AvailableButton.Name = "AvailableButton";
            this.AvailableButton.Size = new System.Drawing.Size(75, 23);
            this.AvailableButton.TabIndex = 3;
            this.AvailableButton.Text = "Available";
            this.AvailableButton.UseVisualStyleBackColor = true;
            this.AvailableButton.Click += new System.EventHandler(this.AvailableButton_Click);
            // 
            // UnavailableButton
            // 
            this.UnavailableButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.UnavailableButton.Location = new System.Drawing.Point(332, 413);
            this.UnavailableButton.Name = "UnavailableButton";
            this.UnavailableButton.Size = new System.Drawing.Size(75, 23);
            this.UnavailableButton.TabIndex = 4;
            this.UnavailableButton.Text = "Unavailable";
            this.UnavailableButton.UseVisualStyleBackColor = true;
            this.UnavailableButton.Click += new System.EventHandler(this.UnavailableButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.Enabled = false;
            this.CancelButton.Location = new System.Drawing.Point(629, 413);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 5;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // RefreshTimer
            // 
            this.RefreshTimer.Interval = 1000;
            this.RefreshTimer.Tick += new System.EventHandler(this.RefreshTimer_Tick);
            // 
            // ScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 448);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.UnavailableButton);
            this.Controls.Add(this.AvailableButton);
            this.Controls.Add(this.CancelCheckBox);
            this.Controls.Add(this.ScheduleCalendar);
            this.Controls.Add(this.ScheduleListView);
            this.Name = "ScheduleForm";
            this.Text = "Meeting Schedule";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ScheduleForm_FormClosed);
            this.Load += new System.EventHandler(this.ScheduleForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ScheduleListView;
        private System.Windows.Forms.ColumnHeader TimeColumnHeader;
        private System.Windows.Forms.ColumnHeader IDColumnHeader;
        private System.Windows.Forms.ColumnHeader NameColumnHeader;
        private System.Windows.Forms.ColumnHeader EventColumnHeader;
        private System.Windows.Forms.MonthCalendar ScheduleCalendar;
        private System.Windows.Forms.CheckBox CancelCheckBox;
        private System.Windows.Forms.Button AvailableButton;
        private System.Windows.Forms.Button UnavailableButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Timer RefreshTimer;
    }
}

