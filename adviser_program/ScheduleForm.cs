using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.IO;
//
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Reflection;
using Outlook = Microsoft.Office.Interop.Outlook;



namespace Meeting_Schedule
{

    public partial class ScheduleForm : Form
    {

        public ScheduleForm()
        {
            InitializeComponent();
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                cxn.Open();
                using (var cmd = new SqlCommand("Queries have been ommitted for security purposes '" + Convert.ToString(System.Security.Principal.WindowsIdentity.GetCurrent().User) + "';", cxn))
                {
                    var rs = cmd.ExecuteReader();
                    if (rs.Read())
                    {
                        Global.iAdvisorID = Convert.ToInt32(rs[0]);
                        Global.strAdvisorDisplayText = Convert.ToString(rs[1]);
                    }
                    else
                    {
                        MessageBox.Show("Queries have been ommitted for security purposes ");
                        Application.Exit();
                    }
                    rs.Close();
                }
                HighestMeetingID_Update(cxn);
                using (var cmd = new SqlCommand("Queries have been ommitted for security purposes ", cxn))
                {
                    var rs = cmd.ExecuteReader();
                    while (rs.Read())
                    {
                        switch (Convert.ToString(rs[0]))
                        {
                            case "Schedule Interval": Global.iScheduleInterval = Convert.ToInt32(rs[1]); break;
                            case "Day Start Time": Global.iDayStartTime = Convert.ToInt32(rs[1]); break;
                            case "Day End Time": Global.iDayEndTime = Convert.ToInt32(rs[1]); break;
                            case "Free Meeting": Global.iFreeMeeting = Convert.ToInt32(rs[1]); break;
                        }
                    }
                    rs.Close();
                }
                cxn.Close();
            }
        }

        private void ScheduleForm_Load(object sender, EventArgs e)
        {
            Global.GetOutlook();
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                cxn.Open();
                ScheduleListView_Update(cxn);
                cxn.Close();

                
            }
            ScheduleCalendar.MinDate = DateTime.Today;
            Global.StartListener();
            RefreshTimer.Enabled = true;
        }

        private void ScheduleForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Global.StopListener();
        }

        private void ScheduleCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                cxn.Open();
                ScheduleListView_Update(cxn);
                cxn.Close();
            }
        }

        private void CancelCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            CancelButton.Enabled = CancelCheckBox.Checked;
        }

        private void AvailableButton_Click(object sender, EventArgs e)
        {
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                cxn.Open();
                for (int i = 0; i < ScheduleListView.Items.Count; i++)
                {
                    if (ScheduleListView.Items[i].Selected)
                    {
                        if (ScheduleListView.Items[i].Tag == null)
                        {
                            using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + Convert.ToString(Global.iAdvisorID) + ", '" + ScheduleCalendar.SelectionStart.AddHours(Global.iDayStartTime / 100 + i / 12).AddMinutes(Global.iDayStartTime % 100 + i % 12 * 5) + "';", cxn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
                ScheduleListView_Update(cxn);
                cxn.Close();
            }
            using (var n = new UdpClient())
            {
                byte[] bytes = Encoding.ASCII.GetBytes("Available");
                n.Send(bytes, bytes.Length, new IPEndPoint(IPAddress.Broadcast, Global.iSendPort));
                n.Close();
            }
        }

        private void UnavailableButton_Click(object sender, EventArgs e)
        {
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                cxn.Open();
                for (int i = 0; i < ScheduleListView.Items.Count; i++)
                {
                    if (ScheduleListView.Items[i].Selected)
                    {
                        int m;
                        
                        if ( (MeetingIdentifier)ScheduleListView.Items[i].Tag != null)
                        {
                            var toPass = (MeetingIdentifier)ScheduleListView.Items[i].Tag;
                            m = toPass.getMeetingID();
                        }
                        else
                        {
                            m = Global.iFreeMeeting;
                        }
                        
                        if (m == Global.iFreeMeeting)
                        {
                            using (var cmd = new SqlCommand("Queries have been ommitted for security purposes  " + Convert.ToString(Global.iAdvisorID) + ", '" + ScheduleCalendar.SelectionStart.AddHours(Global.iDayStartTime / 100 + i / 12).AddMinutes(Global.iDayStartTime % 100 + i % 12 * 5) + "';", cxn))
                            {
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
                ScheduleListView_Update(cxn);
                cxn.Close();
            }
            using (var n = new UdpClient())
            {
                byte[] bytes = Encoding.ASCII.GetBytes("Unavailable");
                n.Send(bytes, bytes.Length, new IPEndPoint(IPAddress.Broadcast, Global.iSendPort));
                n.Close();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
            {
                cxn.Open();
                int last = Global.iFreeMeeting;
                for (int i = 0; i < ScheduleListView.Items.Count; i++)
                {//
                    var toPass = (MeetingIdentifier)ScheduleListView.Items[i].Tag;

                    if (ScheduleListView.Items[i].Selected)
                    {
                        int m = toPass.getMeetingID();
                        if (m != Global.iFreeMeeting && m != last)
                        {
                            using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + Convert.ToString(m) + ";", cxn))
                            {
                                Global.DeleteAppointment(toPass.getMeetingDate());
                                cmd.ExecuteNonQuery();
                            }
                            last = m;
                        }
                    }
                }
                ScheduleListView_Update(cxn);
                
                cxn.Close();
            }
            using (var n = new UdpClient())
            {
                byte[] bytes = Encoding.ASCII.GetBytes("Cancel");
                n.Send(bytes, bytes.Length, new IPEndPoint(IPAddress.Broadcast, Global.iSendPort));
                n.Close();
            }
        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            Global.iTickCount++;
            if (Global.fUpdate || Global.iTickCount == 60)
            {
                using (var cxn = new SqlConnection(Global.strDatabaseConnectionString))
                {
                    cxn.Open();
                    if (HighestMeetingID_Update(cxn)) ScheduleListView_Update(cxn);
                    cxn.Close();
                }
                if (ScheduleCalendar.SelectionStart < DateTime.Today) ScheduleCalendar.SelectionStart = DateTime.Today;
                if (ScheduleCalendar.MinDate < DateTime.Today) ScheduleCalendar.MinDate = DateTime.Today;
            }
            if (Global.iTickCount == 60) Global.iTickCount = 0;
        }

        private void ScheduleListView_Update(SqlConnection cxn)
        {
            if (ScheduleListView.Items.Count == 0)
            {
                for (int i = Global.iDayStartTime; i < Global.iDayEndTime; i += i % 100 == 55 ? 45 : Global.iScheduleInterval)
                {
                    ScheduleListView.Items.Add((i / 100 < 10 ? "0" : "") + Convert.ToString(i / 100) + ":" + (i % 100 < 10 ? "0" : "") + Convert.ToString(i % 100));
                }
            }
            else
            {
                for (int i = 0; i < ScheduleListView.Items.Count; i++)
                {
                    ScheduleListView.Items[i].BackColor = Color.White;
                    for (int j = ScheduleListView.Items[i].SubItems.Count - 1; j > 0; j--)
                    {
                        
                        ScheduleListView.Items[i].SubItems.RemoveAt(j);
                    }
                }
            }
            using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + Convert.ToString(Global.iAdvisorID) + " Queries have been ommitted for security purposes " + ScheduleCalendar.SelectionStart + "Queries have been ommitted for security purposes " + ScheduleCalendar.SelectionStart.AddDays(1) + "Queries have been ommitted for security purposes ", cxn))
            {
                var rs = cmd.ExecuteReader();
                while (rs.Read())
                {
                    int idx = (Convert.ToDateTime(rs[0]).Hour - Global.iDayStartTime / 100) * 12 + (Convert.ToDateTime(rs[0]).Minute / 5 - (Global.iDayStartTime % 100) / 5);
                    
                    
                    MeetingIdentifier tag = new MeetingIdentifier(Convert.ToInt32(rs[1]), Convert.ToDateTime(rs[0]));
                    
                    ScheduleListView.Items[idx].Tag = tag;
                    var toCompare = ScheduleListView.Items[idx].Tag as MeetingIdentifier;
                    var toCompare2 = new MeetingIdentifier(0, DateTime.Now.AddDays(-1));
                    if (ScheduleListView.Items[idx - 1].Tag != null)
                    {
                        toCompare2 = ScheduleListView.Items[idx - 1].Tag as MeetingIdentifier;
                    }
                    
                    if (Convert.ToInt32(tag.getMeetingID()) == Global.iFreeMeeting)
                    {
                        ScheduleListView.Items[idx].BackColor = Color.Green;
                    }
                    else
                    {
                        ScheduleListView.Items[idx].BackColor = Color.Gray;
                        if (idx == 0 || toCompare.getMeetingID() != toCompare2.getMeetingID())
                        {
                            ScheduleListView.Items[idx].SubItems.Add(Convert.ToString(rs[2]));
                            ScheduleListView.Items[idx].SubItems.Add(Convert.ToString(rs[3]));
                            ScheduleListView.Items[idx].SubItems.Add(Convert.ToString(rs[4]));
                            //
                            if (!Global.MeetingExists(Convert.ToDateTime(rs[0])))
                            {
                                Global.AddAppointments(Convert.ToDateTime(rs[0]), Convert.ToString(rs[1]), Convert.ToString(rs[2]), Convert.ToString(rs[3]), Convert.ToString(rs[4]), Convert.ToString(rs[5]), Convert.ToString(rs[6]));  
                            }
                            
                        }
                    }
                }
                rs.Close();
            }
        }

        private bool HighestMeetingID_Update(SqlConnection cxn)
        {
            int i = Global.iHighestMeetingID;
            using (var cmd = new SqlCommand("Queries have been ommitted for security purposes " + Convert.ToString(Global.iAdvisorID) + "Queries have been ommitted for security purposes ", cxn))
            {
                Global.iHighestMeetingID = Convert.ToInt32(cmd.ExecuteScalar());
            }
            return i != Global.iHighestMeetingID;
        }

    }

    class Global
    {

        public const int iSendPort = 121, iReceivePort = 122;

        public const string strDatabaseConnectionString = "server = ommited";

        public static Listener oListener;
        public static Thread oThread;

        public static Outlook.Application outApp;
        public static Outlook.Folder calendarFolder;
        public static Outlook.Items calendarItems;

        public static bool fUpdate = false;
        public static int iTickCount = 0;

        public static int iAdvisorID;
        public static string strAdvisorDisplayText;

        public static int iScheduleInterval;
        public static int iDayStartTime, iDayEndTime;
        public static int iFreeMeeting;

        public static int iHighestMeetingID;

        public static void StartListener()
        {
            oListener = new Listener();
            oThread = new Thread(new ThreadStart(oListener.Run));
            oThread.Start();
        }

        public static void StopListener()
        {
            oThread.Abort();
        }


        //Get active Outlook Instance
        public static void GetOutlook()
        {
            if(Process.GetProcessesByName("OUTLOOK").Count() > 0){
                outApp = Marshal.GetActiveObject("Outlook.Application") as Outlook.Application;
            }
            else
            {
                outApp = new Outlook.Application();
                Outlook.NameSpace nameSpace = outApp.GetNamespace("MAPI");
                nameSpace.Logon("", "", Missing.Value, Missing.Value);
                nameSpace = null;
            }
            calendarFolder = outApp.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderCalendar) as Outlook.Folder;
            calendarItems = calendarFolder.Items;
        }

      //Creates Outlook appointment.
      public static void AddAppointments(DateTime appointmentTime, String meetingID, String studentID, String studentName, String displayText, String studentPhone, String studentEmail)
        {
            try
            {
                Outlook.AppointmentItem newAppointment = (Outlook.AppointmentItem)outApp.CreateItem(Outlook.OlItemType.olAppointmentItem);
                newAppointment.Subject = (Convert.ToString(appointmentTime) + studentName);
                newAppointment.Body = (displayText + "\n" + studentName + "\nPhone: " + studentPhone + "\nStudent Email: " + studentEmail);
                newAppointment.Start = appointmentTime;
                //newAppointment.End = Convert.ToDateTime(appointmentTime);
                newAppointment.ReminderSet = true;
                newAppointment.ReminderMinutesBeforeStart = 15;
                newAppointment.Save();
              
                //**OPTIONAL** 

                //Outlook.MailItem mailItem = newAppointment.ForwardAsVcal;
                //mailItemTo = "***@***";
                //mailItem.Send;
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("The following error occurred: " + ex.Message);
            }
        }


        public static void DeleteAppointment(DateTime toDeleteTimeSlot)
        {
            foreach (Outlook.AppointmentItem appt in calendarItems)
            {
                if (appt.Start.Equals(toDeleteTimeSlot))
                {
                    appt.Delete();
                }
            }
        }

        public static Boolean MeetingExists(DateTime check)
        {
            foreach (Outlook.AppointmentItem appt in calendarItems)
            {
                if (appt.Start.Equals(check))
                {
                    return true;

                }
            }

            return false;
        }

    }

    //to be passed into the Tag of the ListViewItems 
    //adds a unique string (DateTime) to pinpoint the meeting to remove
    public class MeetingIdentifier
    {
        DateTime MeetingDate;
        Int32 iMeetingID;
        public MeetingIdentifier(Int32 iMeetingID, DateTime MeetingDate)
        {
            this.MeetingDate = MeetingDate;
            this.iMeetingID = iMeetingID;
        }

        public DateTime getMeetingDate(){
            return this.MeetingDate;
        }

        public Int32 getMeetingID()
        {
            return this.iMeetingID;
        }
    }

    public class Listener
    {
        public void Run()
        {
            try
            {
                using (var c = new UdpClient(Global.iReceivePort))
                {
                    for (;;)
                    {
                        if (c.Available > 0)
                        {
                            var a = new IPEndPoint(IPAddress.Any, Global.iReceivePort);
                            byte[] d = c.Receive(ref a);
                            string s = Encoding.ASCII.GetString(d);
                            switch (s)
                            {
                                case "Schedule":
                                    Global.fUpdate = true;
                                    break;
                            }
                        }
                        else
                        {
                            Thread.Sleep(100);
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }

    

}
